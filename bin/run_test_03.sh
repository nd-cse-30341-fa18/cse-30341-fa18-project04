#!/bin/bash

# Functions

test-library() {
    library=$1
    echo -n "Testing $library ... "
    if diff -y <(env LD_PRELOAD=./lib/$library ./bin/test_03 2> /dev/null) <($library-output) >& test.log; then
    	echo "success"
    else
    	echo "failure"
    	cat test.log
    	echo ""
    fi
}

libmalloc-ff.so-output() {
    cat <<EOF
blocks:      27
free blocks: 7
mallocs:     30
frees:       10
callocs:     0
reallocs:    0
reuses:      18
grows:       14
shrinks:     0
splits:      13
merges:      0
requested:   5115
heap size:   3944
internal:    0.25
external:    32.63
EOF
}

libmalloc-bf.so-output() {
    cat <<EOF
blocks:      22
free blocks: 2
mallocs:     30
frees:       10
callocs:     0
reallocs:    0
reuses:      18
grows:       18
shrinks:     0
splits:      8
merges:      4
requested:   5115
heap size:   3840
internal:    0.00
external:    3.70
EOF
}

libmalloc-wf.so-output() {
    cat <<EOF
blocks:      29
free blocks: 9
mallocs:     30
frees:       10
callocs:     0
reallocs:    0
reuses:      18
grows:       12
shrinks:     0
splits:      17
merges:      0
requested:   5115
heap size:   3880
internal:    0.26
external:    69.23
EOF
}

# Main execution

test-library libmalloc-ff.so
test-library libmalloc-bf.so
test-library libmalloc-wf.so

# vim: sts=4 sw=4 ts=8 ft=sh
